import Subtitle from './subtitle';

import styles from './header.module.scss';

type Props = {
  title: string;
  subtitle?: string;
};

const Header = (props: Props) => {
  const { title, subtitle } = props;

  return (
    <div className={styles['header']}>
      <h1>{title}</h1>

      {!!subtitle?.length && <Subtitle text={subtitle} />}
    </div>
  );
};

export default Header;
