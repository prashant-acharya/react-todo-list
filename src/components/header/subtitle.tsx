type Props = {
  text?: string;
};

const Subtitle = (props: Props) => {
  const { text } = props;
  return <p>{text}</p>;
};

export default Subtitle;
