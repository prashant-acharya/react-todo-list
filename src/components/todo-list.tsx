import { Todo } from '../types/todo';
import TodoItem from './todo-item';

type Props = {
  todoList: Todo[];
  deleteTodo: (id: number) => void;
  updateTodoStatus: (id: number) => void;
};

const TodoList = (props: Props) => {
  const { todoList, deleteTodo, updateTodoStatus } = props;

  return (
    <ul>
      {todoList.map((item) => (
        <TodoItem
          todo={item}
          deleteTodo={deleteTodo}
          updateTodoStatus={updateTodoStatus}
          key={item.id}
        />
      ))}
    </ul>
  );
};

export default TodoList;
