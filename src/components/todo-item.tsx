import { Todo } from '../types/todo';
import { FaTrashAlt } from 'react-icons/fa';
import { FaCircleCheck, FaXmark } from 'react-icons/fa6';

import Button from '@mui/material/Button';

type Props = {
  todo: Todo;
  deleteTodo: (id: number) => void;
  updateTodoStatus: (id: number) => void;
};

const TodoItem = (props: Props) => {
  const { todo, deleteTodo, updateTodoStatus } = props;

  const handleDelete = () => {
    deleteTodo(todo.id);
  };

  const handleTodoStatusUpdate = () => {
    updateTodoStatus(todo.id);
  };

  return (
    <li>
      <p>{todo.task} </p>

      <div>
        <Button variant="contained" color="error" onClick={handleDelete}>
          <FaTrashAlt />
        </Button>

        <Button
          variant="contained"
          color={todo.completed ? 'error' : 'success'}
          onClick={handleTodoStatusUpdate}
        >
          {todo.completed ? <FaXmark /> : <FaCircleCheck />}
        </Button>
      </div>
    </li>
  );
};

export default TodoItem;
