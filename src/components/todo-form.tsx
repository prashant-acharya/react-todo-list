import { useState } from 'react';

type TodoFormProps = {
  addTodo: (title: string) => void;
};

const TodoForm = (props: TodoFormProps) => {
  const { addTodo } = props;
  const [text, setText] = useState('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setText(event.target.value);
  };

  const handleSubmit = (event: any) => {
    event.preventDefault();
    if (text.length) {
      addTodo(text);
    }

    setText('');
  };

  return (
    <form onSubmit={handleSubmit} className="todo-form">
      <input name="todo" type="text" value={text} onChange={handleChange} />
      <button>Create a Todo</button>
    </form>
  );
};

export default TodoForm;
