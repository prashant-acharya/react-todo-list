import { useEffect, useState } from 'react';
import Pagination from '@mui/material/Pagination';

import type { Todo } from './types/todo';
import Header from './components/header';
import TodoForm from './components/todo-form';
import TodoList from './components/todo-list';

import './App.scss';

const PAGE_SIZE = 2;

function App() {
  const [todos, setTodos] = useState<Todo[]>([]);
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    console.log(todos);
    window.localStorage.setItem('todos', JSON.stringify(todos));
  }, [todos]);

  useEffect(() => {
    const savedData = window.localStorage.getItem('todos');

    if (savedData) {
      const savedTodos = JSON.parse(savedData);
      setTodos(savedTodos);
    }
  }, []);

  const addTodo = (title: string) => {
    setTodos([...todos, { id: Date.now(), task: title, completed: false }]);
  };

  const deleteTodo = (id: number) => {
    const newTodos = todos.filter((todo) => todo.id !== id);
    setTodos(newTodos);
  };

  const updateTodoStatus = (id: number) => {
    const newTodos = todos.map((todo) => {
      if (todo.id === id) {
        return { ...todo, completed: !todo.completed };
      }

      return todo;
    });

    setTodos(newTodos);
  };

  const handlePageChange = (
    _event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    setCurrentPage(value);
  };

  const pageCount = Math.ceil(todos.length / PAGE_SIZE);
  const startIndex = (currentPage - 1) * PAGE_SIZE;
  const endIndex = startIndex + PAGE_SIZE;

  const todosToDisplay = todos.slice(startIndex, endIndex);

  return (
    <div className="main_container_wrapper">
      <div className="container">
        <Header title="Todo List" subtitle="" />

        <TodoForm addTodo={addTodo} />

        <TodoList
          todoList={todosToDisplay}
          deleteTodo={deleteTodo}
          updateTodoStatus={updateTodoStatus}
        />

        {!!todos.length && (
          <div className="pagination-wrapper">
            <Pagination
              count={pageCount}
              page={currentPage}
              onChange={handlePageChange}
              color="primary"
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
